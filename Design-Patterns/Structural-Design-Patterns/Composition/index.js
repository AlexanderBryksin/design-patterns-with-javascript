// Composite pattern !

// Group of item or Trees
// Tree structure of objects
// Class hierarchies with primitive and objects
// Any level of complexity

const Course = require("./Course");
const CourseGroup = require("./CourseGroup");

const jsCourse = new Course("JS", 150);
const goCource = new Course("GO", 120);

const javaCourse = new Course("JAVA", 100);
const kotlinCource = new Course("Kotlin", 100);

const web = new CourseGroup("Web", [jsCourse, goCource]);
const mobile = new CourseGroup("Mobile", [javaCourse, kotlinCource]);

const main_node = new CourseGroup('Main', [web ,mobile]);


web.print();
console.log('$' + web.total());
mobile.print();
console.log('$' + mobile.total());
main_node.print();

let user = 1;

