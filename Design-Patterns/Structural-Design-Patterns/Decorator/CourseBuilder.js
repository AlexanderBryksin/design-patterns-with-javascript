const Course = require("./course");

class CourseBuilder {
  constructor(name, sales = 0, price = 0) {
    this.name = name;
    this.sales = sales;
    this.price = price;
  }

  makePaid(price) {
    this.isFree = false;
    this.price = price;
    return this;
  }

  makeCompain() {
    this.isCampain = true;
    return this;
  }

  build() {
    return new Course(this);
  }
}

class HotNew {
  constructor(baseCourse) {
    this.name = baseCourse.name + " is Hot and New !";
  }
}

class BestSeller {
  constructor(baseCourse) {
    this.name = baseCourse.name + " is Best Seller ! !";
  }
}

module.exports = {
  CourseBuilder,
  HotNew,
  BestSeller
};

const mySuperFunc = payload => async (dispatch, getSate) => {
  const result = await fetch("www.google.com");
  const { data, ...rest } = result;

  return {
    type: "SUCCESS",
    payload: data,
    data: rest
  };
};

const name = 'JS';
mySuperFunc(name)
console.log(name);

