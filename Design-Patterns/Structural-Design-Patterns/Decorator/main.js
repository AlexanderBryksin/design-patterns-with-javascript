const { CourseBuilder, BestSeller, HotNew } = require("./CourseBuilder");

// const course_1 = new CourseBuilder('Design Patterns 1', 0, true, 149 , true);
// const course_2 = new CourseBuilder('Design Patterns 1', 0,false, 0, false);

const course_1 = new CourseBuilder("Design Patterns 1")
  .makePaid(100)
  .makeCompain()
  .build();

const hotNew_1 = new HotNew(course_1);
const bestSeller_1 = new BestSeller(course_1);

course_1.toString(course_1);
course_1.toString(hotNew_1);
course_1.toString(bestSeller_1);
