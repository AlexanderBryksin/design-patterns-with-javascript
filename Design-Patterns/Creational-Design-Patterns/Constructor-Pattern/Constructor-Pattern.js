// Constructor Pattern
// new Syntax ES6
class Course {
  constructor(title, author) {
    this.title = title;
    this.author = author;
  }

  toString() {
    return console.log(this.title + "... Author: " + this.author);
  }
}

const newCourse = new Course("TS", "Alex");
newCourse.toString();

// Old JS Syntax
// let Course = function(title, author) {
//   this.title = title;
//   this.author = author;
//
//   this.toString = function() {
//     return this.title + "... Author: " + this.author;
//   };
// };
//
// let course1 = new Course("JAVA", "Alex");
// let course2 = new Course("JS", "Alexander");
//
// console.log(course1.toString());


