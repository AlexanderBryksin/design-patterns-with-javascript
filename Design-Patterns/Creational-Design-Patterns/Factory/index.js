// Factory pattern

// 1. To separate the object creation from its implementation
// 2. To create different instance based on a condition
// 3. Not expose the constructors of the objects, preventing their modification

const userFactory = require('./userFactory');

const alex = userFactory('instructor', 'Alexander', 'Software Engineer', 1000);
const john = userFactory('student', 'John', 'Beginner');

console.log(alex.toString());
console.log(john.toString());
