// Constructor-prototype

// ES5 Version
let Course = function(title, author) {
  this.title = title;
  this.author = author;
};

Course.prototype.toString = function(arguments) {
  return console.log(this.title + "... Author: " + this.author)
};

let course1 = new Course("JAVA", "Alex");
let course2 = new Course("JS", "Alexander");

course1.toString();
course2.toString();