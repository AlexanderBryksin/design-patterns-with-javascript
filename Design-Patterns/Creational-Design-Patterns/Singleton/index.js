// Singleton

// 1. Restricts instantiations of a class to a single object
// 2. Keeps the same instance of that object
// 3. Implementation based on the condition => "Only one instance"
// Dont create more than one time

const Singleton = (() => {
  let course;

  const assignCourse = () => {
    const course = new Object("JS");
    return course;
  };

  return {
    getInstance: () => {
      if (!course) {
        course = assignCourse;
      }
      return course;
    }
  };
})();

const buyFirstTime = Singleton.getInstance();
const buySecondTime = Singleton.getInstance();

if (buyFirstTime === buySecondTime) {
  console.log("Go to course");
}



























