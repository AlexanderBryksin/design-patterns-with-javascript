const CourseDb = () => {
  return {
    dbInfo: course => {
      console.log(
        "Course id: " +
          course.id +
          " Course title: " +
          course.title +
          "author: " +
          course.author
      );
    }
  };
};

module.exports = CourseDb();
