const CourseDb = require('./CourseDb');

class Course {
  constructor(data) {
    this.title = data.title;
    this.author = data.author;
    this.id = data.id
  }

  DBRequest() {
    CourseDb.dbInfo(this);
  }

}

module.exports = Course;